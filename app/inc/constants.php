<?php
/**
 * This software is governed by the CeCILL-B license. If a copy of this license
 * is not distributed with this file, you can obtain one at
 * http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.txt
 *
 * Authors of STUdS (initial project): Guilhem BORGHESI (borghesi@unistra.fr) and Raphaël DROZ
 * Authors of Framadate/OpenSondate: Framasoft (https://github.com/framasoft)
 *
 * =============================
 *
 * Ce logiciel est régi par la licence CeCILL-B. Si une copie de cette licence
 * ne se trouve pas avec ce fichier vous pouvez l'obtenir sur
 * http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.txt
 *
 * Auteurs de STUdS (projet initial) : Guilhem BORGHESI (borghesi@unistra.fr) et Raphaël DROZ
 * Auteurs de Framadate/OpenSondage : Framasoft (https://github.com/framasoft)
 */

// FRAMADATE version
const VERSION = 0.8;

// Server name
const STUDS_URL = 'local.sandstorm.io:6080';

// Application name
const NOMAPPLICATION = "Framadate";

// Database administrator email
const ADRESSEMAILADMIN = 'larjona@larjona.net';

// Email for automatic responses (you should set it to "no-reply")
const ADRESSEMAILREPONSEAUTO = 'larjona@larjona.net';

// Database name
const BASE = 'framadate';

// Database user
const USERBASE = "framadate";

// Database password
const USERPASSWD = 'framadate';

// Database server name, leave empty to use a socket
const SERVEURBASE = '';

// Database type (mysql, postgres…) http://phplens.com/lens/adodb/docs-adodb.htm#drivers
const BASE_TYPE = 'mysql';

// Default Language using POSIX variant of BC P47 standard (choose in $ALLOWED_LANGUAGES)
const LANGUE = 'en_GB';

// List of supported languages, fake constant as arrays can be used as constants only in PHP >=5.6
$ALLOWED_LANGUAGES = [
    'fr_FR' => 'Français',
    'en_GB' => 'English',
    'es_ES' => 'Español',
    'de_DE' => 'Deutsch',
    'it_IT' => 'Italiano',
];

// Path to logo
const LOGOBANDEAU = 'images/logo-framadate.png';

// Path to logo in PDF export
const LOGOLETTRE = 'images/logo-framadate.png';

// Nom et emplacement du fichier image contenant le titre
const IMAGE_TITRE = 'images/logo-framadate.png';

// Clean URLs, boolean
const URL_PROPRE = false;

// Use REMOTE_USER data provided by web server
const USE_REMOTE_USER =  true;

const COMMENT_EMPTY         = 0x0000000001;
const COMMENT_USER_EMPTY    = 0x0000000010;
const COMMENT_INSERT_FAILED = 0x0000000100;
const NAME_EMPTY            = 0x0000001000;
const NAME_TAKEN            = 0x0000010000;
const NO_POLL               = 0x0000100000;
const NO_POLL_ID            = 0x0001000000;
const INVALID_EMAIL         = 0x0010000000;
const TITLE_EMPTY           = 0x0100000000;
const INVALID_DATE          = 0x1000000000;

// Config
$config = [
    /* general config */
    'use_smtp' => false,                     // use email for polls creation/modification/responses notification
    /* home */
    'show_what_is_that' => false,            // display "how to use" section
    'show_the_software' => false,            // display technical information about the software
    'show_cultivate_your_garden' => false,   // display "developpement and administration" information
    /* choix_autre.php / choix_date.php */
    'default_poll_duration' => 180,          // default values for the new poll duration (number of days).
    /* choix_autre.php */
    'user_can_add_img_or_link' => true,      // user can add link or URL when creating his poll.
];

